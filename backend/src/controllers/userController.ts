import asyncHandler from "../middleware/async";
import { Request, Response, NextFunction } from "express";
import ErrorResponse from "../utils/errorResponse";
import prisma from "../config/db";

//@desc
//@route		GET /api/v1/user
//@access		Public

export const getUsers = asyncHandler(
  async (_req: Request, res: Response, _next: NextFunction) => {
    const users = await prisma.user.findMany();

    res.status(200).json({
      success: true,
      data: users,
    });
  }
);

//@route		POST /api/v1/user
//@access		Public

export const createUser = asyncHandler(
  async (req: Request, res: Response, next: NextFunction) => {
    const { name, email } = req.body;

    if (!email || !name) {
      return next(
        new ErrorResponse("Please provide an email and password", 400)
      );
    }

    // const user = await prisma.user.create({
    //   data: {
    //     name,
    //     email,
    //   },
    // });

    // console.log(user);

    res.status(200).json({
      success: true,
      // data: user,
    });
  }
);
