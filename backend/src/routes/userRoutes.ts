import { Router } from "express";
import { createUser, getUsers } from "../controllers/userController";

const router = Router({ mergeParams: true });

router.get("/", getUsers);
router.post("/create", createUser);

export = router;
