import dotenv from "dotenv";
import express from "express";
import helmet from "helmet";
import path from "path";
import config from "./config/config";
import morgan from "morgan";
import cors from "cors";

// routes
import { BASE_ROUTE, USER_ROUTE } from "./constants/routes";
import userRoutes from "./routes/userRoutes";

dotenv.config({ path: path.join(__dirname, "../.env") });
config(process.env.NODE_ENV || "development");

const main = async () => {
  const app = express();

  // Express body parser
  app.use(express.urlencoded({ extended: true }));
  app.use(express.json());

  if (process.env.NODE_ENV === "development") {
    app.use(morgan("dev"));
  }

  // Set security headers
  app.use(helmet());

  // Enable CORS
  app.use(cors());

  app.use(`${BASE_ROUTE}${USER_ROUTE}`, userRoutes);

  app.get("/", (_, res) => {
    res.status(200).send({
      status: "connected successfully",
    });
  });

  app.listen(process.env.PORT || 5000, () => {
    console.log(
      `Server listening in ${process.env.NODE_ENV} on ${process.env.PORT}`
    );
  });
};

main();